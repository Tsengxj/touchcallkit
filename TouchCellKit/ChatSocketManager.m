//
//  ChatSocketManager.m
//  Betelgeuse
//
//  Created by 曾宪杰 on 2020/6/12.
//  Copyright © 2020 m42.x. All rights reserved.
//

#import "ChatSocketManager.h"
#import "MSWeakTimer/MSWeakTimer.h"
#import "SocketRocket/SocketRocket.h"

// notif key
static NSString *const ChatStatus = @"ChatStatus";

// 对应SocketStatus
NSString *const kNotificationChatSocketStatus = @"kNotificationChatSocketStatus";
// 消息回调
NSString *const kNotificationChatSocketDidReceive = @"kNotificationChatSocketDidReceive";
// 失败回调
NSString *const kNotificationChatSocketDidFail = @"kNotificationChatSocketDidFail";

@interface ChatSocketManager ()<SRWebSocketDelegate>
@property (nonatomic,strong)SRWebSocket *webSocket;
@property (nonatomic,assign) SocketStatus x_socketStatus;
//@property (nonatomic,weak) NSTimer *timer; // 换成msweaktimer
@property (nonatomic,copy) NSString *urlString;
@property (nonatomic,strong) dispatch_queue_t timerQueue;
@property (nonatomic,strong) MSWeakTimer *timer;
@end

@implementation ChatSocketManager
{
    NSInteger _reconnectCounter; // 私有重连次数
}

+ (instancetype)shareManager {
    static ChatSocketManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.overTime = 1;
        instance.reConncectCount = 5;
        instance.timerQueue = dispatch_queue_create("come.ChatSocketManager.Queue", NULL);
        
    });
    
    return instance;
}

// block open
- (void)wssOpen:(NSString *)urlStr connect:(SocketDidConnectBlock)conect receive:(SocketDidReceiveBlock)receive failure:(SocketDidFailBlock)failure {
    [ChatSocketManager shareManager].connect = conect;
    [ChatSocketManager shareManager].receive = receive;
    [ChatSocketManager shareManager].failure = failure;
    [self aOpen:urlStr];
}
// block close
- (void)wssClose:(SocketDidCloseBlock)close {
    [ChatSocketManager shareManager].close = close;
    [self aClose];
}

// notif open
- (void)wssOpen:(NSString *)urlStr {
    [self aOpen:urlStr];
}

- (void)wssClose {
    [self aClose];
    NSDictionary *userInfo = @{
        ChatStatus:@"SocketStatusClosed"
    };
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatSocketStatus object:userInfo];
}

- (void)wssSend:(id)data {
   SocketStatus status = [ChatSocketManager shareManager].status;
    switch (status) {
        case SocketStatusConnected:
        case SocketStatusReceived: {
           
            NSError *err = nil;
            [self.webSocket sendData:data error:&err];
            NSLog(@"发送中。。。error = %@\n",err.description);
            break;
        }
        
        case SocketStatusFailed: {
            NSLog(@"发送失败\n");
            break;
        }
        case SocketStatusCloseByServer: {
            NSLog(@"SocketStatusCloseByServer 已关闭\n");
        }
            
        case SocketStatusCloseByUser: {
            NSLog(@"SocketStatusCloseByUser 已关闭\n");
        }
            
        default:
            break;
    }
}

#pragma mark -- private method
- (void)aOpen:(id)params {
    NSString *urlStr = @"";
    if ([params isKindOfClass:[NSString class]]) {
        urlStr = (NSString *)params;
    } else if ([params isKindOfClass:[NSTimer class]]) {
//        NSTimer *timer = (NSTimer *)params;
        MSWeakTimer *timer = (MSWeakTimer *)params;
        urlStr = [timer userInfo];
    }
    
    [ChatSocketManager shareManager].urlString = urlStr;
    [self.webSocket close];
    self.webSocket.delegate = nil;
    
    self.webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    self.webSocket.delegate = self;
    
    [self.webSocket open];
}

- (void)aClose {
    [self.webSocket close];
    self.webSocket = nil;
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
}

- (void)aReconnect {
    
    if (_reconnectCounter < self.reConncectCount -1) {
        _reconnectCounter++;
        
        // 定时器
        [self startRefreshTimer];
    }
    else {
        NSLog(@"Websocket Reconnected Outnumber ReconnectCount");
        [self stopRefreshTimer];
        
        return;
    }
    
}

- (void)stopRefreshTimer {
    if (self.timer) {
       [self.timer invalidate];
       self.timer = nil;
    }
}

- (void)startRefreshTimer {
    [self stopRefreshTimer];
    
    self.timer = [MSWeakTimer scheduledTimerWithTimeInterval:self.overTime target:self selector:@selector(aOpen:) userInfo:self.urlString repeats:NO dispatchQueue:self.timerQueue];
}

#pragma mark - SRWebSocketDelegate
- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"Websocket connected\n");
    
    [ChatSocketManager shareManager].connect ? [ChatSocketManager shareManager].connect() : @"-1";
    [ChatSocketManager shareManager].x_socketStatus = SocketStatusConnected;
    _reconnectCounter = 0;
    
    NSDictionary *userInfo = @{
        ChatStatus:@"SocketStatusConnected"
    };
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatSocketStatus object:userInfo];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"Websocket failed %@",error);
    
    [ChatSocketManager shareManager].x_socketStatus = SocketStatusFailed;
    [ChatSocketManager shareManager].failure ? [ChatSocketManager shareManager].failure(error) : @"-1";
    
    // 重连
    [self aReconnect];
    
    NSDictionary *userInfo = @{
        ChatStatus:@"SocketStatusFailed"
    };
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatSocketDidFail object:userInfo];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"Websocket receive msg %@\n",message);

    [ChatSocketManager shareManager].x_socketStatus = SocketStatusReceived;
    [ChatSocketManager shareManager].receive ? [ChatSocketManager shareManager].receive(message,SocketReceiveTypeMsg) : @"-1";
    
    NSDictionary *userInfo = @{
        ChatStatus:message
    };
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatSocketDidReceive object:userInfo];
    
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"Websocket close reason %@ code %zd\n",reason,code);
    
    NSDictionary *userInfo = nil;
    
    if (reason) {
        [ChatSocketManager shareManager].x_socketStatus = SocketStatusCloseByServer;
        userInfo = @{ChatStatus:@"SocketStatusCloseByServer"};
        
        [self aReconnect];
    }
    else {
        [ChatSocketManager shareManager].x_socketStatus = SocketStatusCloseByUser;
        userInfo = @{ChatStatus:@"SocketStatusCloseByUser"};
    }
    
    [ChatSocketManager shareManager].close ? [ChatSocketManager shareManager].close(code,reason,wasClean) : @"-1";
    self.webSocket = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChatSocketStatus object:userInfo];
}

- (void)dealloc {
    [self aClose];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
