//
//  ChatSocketManager.h
//  Betelgeuse
//
//  Created by 曾宪杰 on 2020/6/12.
//  Copyright © 2020 m42.x. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,SocketStatus) {
    SocketStatusConnected,
    SocketStatusFailed,
    SocketStatusCloseByServer,
    SocketStatusCloseByUser,
    SocketStatusReceived
};

typedef NS_ENUM(NSInteger,SocketReceiveType) {
    SocketReceiveTypeMsg,
    SocketReceiveTypePong
};

// 对应SocketStatus
extern NSString *const kNotificationChatSocketStatus;
// 消息回调
extern NSString *const kNotificationChatSocketDidReceive;
// 失败回调
extern NSString *const kNotificationChatSocketDidFail;


typedef void(^SocketDidConnectBlock)(void);
typedef void(^SocketDidFailBlock)(NSError *err);
typedef void(^SocketDidCloseBlock)(NSInteger code,NSString *reason,BOOL wasClean);
typedef void(^SocketDidReceiveBlock)(id message,SocketReceiveType recType);

@interface ChatSocketManager : NSObject

@property (nonatomic,copy) SocketDidConnectBlock connect;
@property (nonatomic,copy) SocketDidReceiveBlock receive;
@property (nonatomic,copy) SocketDidFailBlock failure;
@property (nonatomic,copy) SocketDidCloseBlock close;
@property (nonatomic,assign,readonly) SocketStatus status;

@property (nonatomic,assign) NSTimeInterval overTime; // 超时
@property (nonatomic,assign) NSUInteger reConncectCount; //重连
- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)shareManager;
// Notification 
- (void)wssOpen:(NSString *)urlStr;
- (void)wssClose;

// block
- (void)wssOpen:(NSString*)urlStr
        connect:(SocketDidConnectBlock)conect
        receive:(SocketDidReceiveBlock)receive
        failure:(SocketDidFailBlock)failure;

- (void)wssClose:(SocketDidCloseBlock)close;

// 发送
- (void)wssSend:(id)data;

@end

NS_ASSUME_NONNULL_END
