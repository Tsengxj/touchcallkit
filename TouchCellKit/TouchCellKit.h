//
//  TouchCellKit.h
//  TouchCellKit
//
//  Created by 曾宪杰 on 2020/6/12.
//  Copyright © 2020 m42.x. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TouchCellKit.
FOUNDATION_EXPORT double TouchCellKitVersionNumber;

//! Project version string for TouchCellKit.
FOUNDATION_EXPORT const unsigned char TouchCellKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TouchCellKit/PublicHeader.h>

#import <TouchCellKit/ChatSocketManager.h>
